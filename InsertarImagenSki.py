# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Lectura de Imagen con Scikit-Imagen
# Descripción: Visualizar imagen desde un directorio interno


from skimage import io

#leer imagen con scikit-image
img = io.imread("./imagenes/lena.png")
#Mostrar Imagen
io.imshow(img)
io.show()