# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Lectura de Imagen con OpenCV
# Descripción: Visualizar imagen desde un directorio interno

#Importar Libreria OpenCV
import cv2 as cv

#Leer imagen con Libreria con OpenCV
imagen =cv.imread('./imagenes/lena.png')
#Muestra pantalla con titulo
cv.imshow('Lena',imagen)

#Espera presionar una tecla para destruir una ventana
cv.waitKey(0)

#Cierra las ventanas
cv.destroyAllWindows()