# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Ingenieria en Computación Gráfica
# Nombre: Sandra Ipiales
# Diciembre 2020
# Lectura de Imagen con Matplotñib
# Descripción: Visualizar imagen desde un directorio interno

#Importar libreria marplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


#leer imagen con matplotlib
img=mpimg.imread('./imagenes/lena.png')
#Mostrar imagen con Matplotlib
imgplot = plt.imshow(img)
plt.show()